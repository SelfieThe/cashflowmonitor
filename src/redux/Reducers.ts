import { combineReducers } from "redux"; // для объединения редьюсеров

export interface Transaction { // Определяем интерфейс для транзакций
  id: number;
  amount: number;
  description: any;
  isExpense: boolean;
}

interface AppState { // Определяем тип для состояния приложения
  income: Transaction[];
  expenses: Transaction[];
}

const initialAppState: AppState = { // Создаем начальное состояние приложения
  income: [], // массив транзакций дохода пуст
  expenses: [], // массив транзакций расходов пуст
};

// Редьюсер для массива транзакций дохода
const incomeReducer = (state = initialAppState.income, action: any) => {
  switch (action.type) {
    case "ADD_INCOME":
      return [...state, action.payload]; // добавляем новую транзакцию дохода в конец массива
    case "DELETE_INCOME":
      return state.filter((income) => income.id !== action.payload); // удаляем транзакцию дохода из массива по id
    case "UPDATE_INCOME":
      return state.map((income) =>
        income.id === action.payload.id ? action.payload : income // обновляем транзакцию дохода с заданным id
      );
    default:
      return state;
  }
};

const expenseReducer = (state = initialAppState.expenses, action: any) => { // Редьюсер для массива транзакций расходов
  switch (action.type) {
    case "ADD_EXPENSE":
      return [...state, action.payload];  // добавляем новую транзакцию расхода в конец
    case "DELETE_EXPENSE":
      return state.filter((expense) => expense.id !== action.payload); // удаляем транзакцию расхода из массива по id
    case "UPDATE_EXPENSE":
      return state.map((expense) =>
        expense.id === action.payload.id ? action.payload : expense // обновляем транзакцию расхода с заданным id
      );
    default:
      return state;
  }
};

// Объединяем редьюсеры с помощью combineReducers
const rootReducer = combineReducers({
  income: incomeReducer,
  expenses: expenseReducer,
});

// Экспортируем rootReducer и тип для состояния приложения
export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>;