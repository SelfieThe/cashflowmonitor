import { Transaction } from "./Reducers";

export const addIncome = (transaction: Transaction) => ({
  type: "ADD_INCOME",
  payload: transaction,
});

export const deleteIncome = (id: number) => ({
  type: "DELETE_INCOME",
  payload: id,
});

export const updateIncome = (transaction: Transaction) => ({
  type: "UPDATE_INCOME",
  payload: transaction,
});


export const addExpense = (transaction: Transaction) => ({
  type: "ADD_EXPENSE",
  payload: transaction,
});

export const deleteExpense = (id: number) => ({
  type: "DELETE_EXPENSE",
  payload: id,
});

export const updateExpense = (transaction: Transaction) => ({
  type: "UPDATE_EXPENSE",
  payload: transaction,
});

