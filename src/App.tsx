import React, { useState } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store  from './redux/Store';
import {Header} from "./components/header/Header";
import Balance from "./components/balance/Balance";
import TransactionForm from "./components/transaction_form/TransactionForm";
import TransactionTable from "./components/transaction_table/TransactionTable";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Header />
        <Balance />
        <TransactionForm />
        <TransactionTable />
      </div>
    </Provider>
  );
}

export default App;
