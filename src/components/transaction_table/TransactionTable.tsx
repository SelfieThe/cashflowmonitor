import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState, Transaction } from "../../redux/Reducers"; //импортируем типы состояния и транзакции из Redux
import {deleteExpense, updateExpense, updateIncome, deleteIncome} from "../../redux/Actions"; //импортируем экшены для работы с Redux

const TransactionTable: React.FC = () => { //компонент таблицы транзакций
  const { income, expenses } = useSelector((state: RootState) => state); //получаем состояние доходов и расходов из Redux
  const dispatch = useDispatch();
  const [selectedTransaction, setSelectedTransaction] = useState<
    Transaction | null
    >(null); // создаем состояние для отслеживания выбранной транзакции

  const handleDelete = (transaction: Transaction) => { //обработчик удаления транзакции
    if (transaction.isExpense) { //если транзакция - расход
      dispatch(deleteExpense(transaction.id)); //отправляем экшен на удаление расхода
    } else { //иначе
      dispatch(deleteIncome(transaction.id)); //отправляем экшен на удаление дохода
    }
  };

  const handleEdit = (transaction: Transaction) => { //обработчик редактирования транзакции
    const newTransaction: Transaction = { //создаём новую транзакцию
      ...transaction,
      amount: Number(prompt("Enter new amount:", transaction.amount.toString())), //получаем новое значение для суммы транзакции
      description: prompt("Enter new description:", transaction.description), //получаем новое значение для описания транзакции
    };
    if (transaction.isExpense) { //если транзакция - расход
      dispatch(updateExpense(newTransaction)); //отправляем экшен на обновление расхода
    } else {
      dispatch(updateIncome(newTransaction)); //отправляем экшен на обновление дохода
    }
  };

  // Функция обработки клика на строке таблицы
  const handleRowClick = (transaction: Transaction) => {
    setSelectedTransaction(transaction); // сохраняем выбранную транзакцию в состояние
  };

  // Функция закрытия модального окна
  const handleCloseModal = () => {
    setSelectedTransaction(null); // сбрасываем выбранную транзакцию
  };

  return (
    <div>
      <h2>Transaction Table</h2>
      <table>
        <thead>
        <tr>
          <th>ID</th>
          <th>Amount</th>
          <th>Description</th>
          <th>Type</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        {/* Добавляем строки для доходных транзакций */}
        {income.map((transaction) => (
          <tr
            key={transaction.id}
            onClick={() => handleRowClick(transaction)} // добавляем обработчик клика на строке
          >
            <td>{transaction.id}</td>
            <td>{transaction.amount}</td>
            <td>{transaction.description}</td>
            <td>Income</td>
            <td>
              <button onClick={() => handleEdit(transaction)}>Edit</button>
              <button onClick={() => handleDelete(transaction)}>Delete</button>
            </td>
          </tr>
        ))}
        {/* Add rows for expense transactions */}
        {expenses.map((transaction) => (
          <tr
            key={transaction.id}
            onClick={() => setSelectedTransaction(transaction)}
          >
            <td>{transaction.id}</td>
            <td>{transaction.amount}</td>
            <td>{transaction.description}</td>
            <td>Expense</td>
            <td>
              <button onClick={() => handleEdit(transaction)}>Edit</button>
              <button onClick={() => handleDelete(transaction)}>Delete</button>
            </td>
          </tr>
        ))}
        </tbody>
      </table>
      {selectedTransaction && (
        <div className="modal">
          <div className="modal-content">
            <span className="close" onClick={() => setSelectedTransaction(null)}>×</span>
            <h3>Transaction Details</h3>
            <p>ID: {selectedTransaction.id}</p>
            <p>Amount: {selectedTransaction.amount}</p>
            <p>Description: {selectedTransaction.description}</p>
            <p>Type: {selectedTransaction.isExpense ? 'Expense' : 'Income'}</p>
          </div>
        </div>
      )}
    </div>
  );
};

export default TransactionTable;

//Не знаю зачем я это комментирую, всёравно никто это не прочитает :)