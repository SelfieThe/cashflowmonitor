import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/Reducers";

interface Transaction { // Интерфейс для транзакций
  id: number;
  amount: number;
  description: string;
}

const Balance: React.FC = () => {
  const { income, expenses } = useSelector((state: RootState) => state);

  const calculateTotal = (transactions: Transaction[]) => { // Функция для вычисления общей суммы транзакций
    return transactions.reduce((acc, transaction) => Number(acc) + Number(transaction.amount), 0);
  };

  // Вычисляем общую сумму доходов, расходов и баланс
  const totalIncome = calculateTotal(income);
  const totalExpenses = calculateTotal(expenses);
  const balance = totalIncome - totalExpenses;

  return (
    <div className="balance-container">
      <h2>Balance</h2>
      <div className="totals-container">
        <h3>Total Income: {totalIncome}</h3>
        <h3>Total Expenses: {totalExpenses}</h3>
        <h3>Balance: {balance}</h3>
      </div>
    </div>
  );
};

export default Balance;