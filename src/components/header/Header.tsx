//компонент, который будет отображать заголовок приложения
import React from 'react';

export const Header: React.FC = () => {
  return (
    <header>
      <h1>Приложение для управления финансами</h1>
    </header>
  );
}