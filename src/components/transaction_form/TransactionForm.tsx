import React, { useState } from "react";
import { useDispatch } from "react-redux"; // Хук для получения функции dispatch из Redux
import { addIncome, addExpense, updateExpense } from "../../redux/Actions"; // Импорт функций для добавления/обновления транзакций
import { Transaction } from "../../redux/Reducers"; // Импорт типа данных Transaction из Redux

interface TransactionFormProps {
  transaction?: Transaction; // Определяем свойство transaction типа Transaction, которое может быть необязательным
}
// Компонент формы добавления/редактирования транзакции
const TransactionForm: React.FC<TransactionFormProps> = ({
                                                           transaction,
                                                         }) => {
  const dispatch = useDispatch();
  const [amount, setAmount] = useState(transaction?.amount.toString() ?? ""); // Используем хук useState для установки состояний для введенной суммы
  const [description, setDescription] = useState(transaction?.description ?? ""); // Используем хук useState для установки состояний для введенного описания
  const [isExpense, setIsExpense] = useState(transaction?.isExpense ?? true); // Используем хук useState для установки состояний для выбранного типа транзакции
  const [showModal, setShowModal] = useState(false)

// Функция-обработчик отправки формы
  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault(); // Отменяем стандартное поведение браузера по отправке формы
    const newTransaction: Transaction = { // Создаем объект новой транзакции
      id: transaction?.id ?? Date.now(), // Устанавливаем id транзакции
      amount: parseFloat(amount), // Устанавливаем сумму транзакции, приведенную к числу с плавающей точкой
      description, // Устанавливаем описание транзакции
      isExpense, // Устанавливаем тип транзакции (расход или доход)
    };
    if (transaction) { // Если передана транзакция, то это редактирование
      dispatch(updateExpense(newTransaction)); // Отправляем запрос на обновление транзакции
    } else { // Иначе это добавление новой транзакции
      if (isExpense) { // Если выбран тип "расход"
        dispatch(addExpense(newTransaction)); // Отправляем запрос на добавление нового расхода
      } else { // Иначе это доход
        dispatch(addIncome(newTransaction)); // Отправляем запрос на добавление нового дохода
      }
    }
  };

  const handleCloseModal = () => {
    setShowModal(false); // Скрываем модальное окно
    setAmount(""); // Сбрасываем состояние суммы в поле ввода
    setDescription(""); // Сбрасываем состояние описания в поле ввода
    setIsExpense(true); // Сбрасываем состояние типа транзакции на расход
  };

  // Возвращаем разметку формы добавления/редактирования транзакции
  return (
    <>
      <button onClick={()=>setShowModal(true)}>Add</button>
      {showModal && <div className="modal">
        <div className="modal-content">
          <h2>{transaction ? "Edit Transaction" : "Add Transaction"}</h2>
          {/* Форма для ввода данных транзакции */}
          <form onSubmit={handleSubmit}>
            <div>
              <label htmlFor="amount">Amount:</label>
              {/* Поле ввода суммы транзакции */}
              <input
                type="number"
                id="amount"
                value={amount}
                onChange={(e) => setAmount(e.target.value)}
                required
              />
            </div>
            <div>
              <label htmlFor="description">Description:</label>
              {/* Поле ввода описания транзакции */}
              <input
                type="text"
                id="description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </div>
            <div>
              <label htmlFor="type">Type:</label>
              {/* Выбор типа транзакции - доход или расход */}
              <select
                id="type"
                value={isExpense ? "expense" : "income"}
                onChange={(e) => setIsExpense(e.target.value === "expense")}
              >
                <option value="expense">Expense</option>
                <option value="income">Income</option>
              </select>
            </div>
            {/* Кнопка для отправки формы */}
            <button type="submit">
              {/* Изменение текста на кнопке в зависимости от того, редактируется ли транзакция или добавляется новая */}
              {transaction ? "Save Changes" : "Add Transaction"}
            </button>
            {/* Кнопка для закрытия модального окна */}
            <button type="button" onClick={handleCloseModal}>Close</button>
          </form>
        </div>
      </div> }
    </>
  );
};

export default TransactionForm;

//Не знаю зачем я это комментирую, всёравно никто это не прочитает :)